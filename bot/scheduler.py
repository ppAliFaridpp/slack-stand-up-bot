import json
import celery
from celery.schedules import crontab
from decouple import config
from logic import cache
from bot.handlers import DailyFSMHandler
from bot.db import reports_col

_ONE_MINUTE_IN_SECONDS = 60
_CELERY_NODE = config('CELERY_NODE', default='schedulers', cast=str)
_CELERY_BROKER_URL = config('CELERY_BROKER_URL', default='pyamqp://guest@localhost//', cast=str)
_TRIGGER_HOUR = config('TRIGGER_HOUR', default=15, cast=int)
_TRIGGER_MINUTE = config('TRIGGER_MINUTE', default=0, cast=int)

app = celery.Celery(_CELERY_NODE, broker=_CELERY_BROKER_URL)


@app.task
def start_reports():
    fsm_list = cache.list_all_fsm()
    # all go to idle and then to start
    for fsm_id in fsm_list:
        DailyFSMHandler.handle_event(user_id=fsm_id, text='reset_')
        DailyFSMHandler.handle_event(user_id=fsm_id, text='alarm_')


@app.task
def broadcast_undone():
    # broadcast to channel undone
    pass


app.conf.timezone = 'Asia/Tehran'
app.conf.beat_schedule = {
    'update-values': {
        'task': 'client.schedulers.handlebeat',
        'schedule': crontab(hour=_TRIGGER_HOUR, minute=_TRIGGER_MINUTE)
    }
}
