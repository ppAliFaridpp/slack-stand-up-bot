import logging
from flask import Flask
from slack_sdk.web import WebClient
from decouple import config
from slackeventsapi import SlackEventAdapter
from bot.handlers import DailyFSMHandler

_BOT_SIGNING_SECRETE = config('BOT_SIGNING_SECRETE', default='', cast=str)

app = Flask(__name__)
slack_events_adapter = SlackEventAdapter(_BOT_SIGNING_SECRETE, "/slack/events", app)


@slack_events_adapter.on('message')
def message(payload):
    event = payload.get('event', {})
    user_id = event.get('user')
    text = event.get('text')
    print(text.lower())
    DailyFSMHandler.handle_event(user_id=user_id, text=text)
