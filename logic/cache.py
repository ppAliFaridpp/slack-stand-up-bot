import json
from decouple import config
from redis import StrictRedis
from logic import fsm

client = StrictRedis(host=config('REDIS_HOST', default='localhost', cast=str),
                     port=config('REDIS_PORT', default=6379, cast=int))


def serialize_key(key):
    return 'user_{}'.format(key)


def retrieve_fsm(key):
    ser_key = serialize_key(key)
    machine_report_json = client.get(ser_key)
    if not machine_report_json:
        return None
    machine_report = json.loads(machine_report_json)
    current_state = fsm.State.get(machine_report['current_state'])[0]
    return fsm.StateMachine.build(machine_report['name'], current_state)


def save_fsm(key, machine: fsm.StateMachine):
    ser_key = serialize_key(key)
    return client.set(ser_key, json.dumps(machine.__repr__()))


def list_all_fsm():
    return client.keys(serialize_key('*'))


def list_undone_fsm():
    all_fsm = list_all_fsm()
    return [fsm_id for fsm_id in all_fsm if json.loads(client.get(fsm_id))['current_state'] != 'DONE']
